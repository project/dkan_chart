<?php

namespace Drupal\dkan_chart\Command;

use Drupal\Core\Ajax\BaseCommand;
use Drupal\Core\Ajax\InvokeCommand;

class DkanVisualizeCommand extends BaseCommand {

  /**
   * @param $command
   * @param mixed $data
   */
  public function __construct($command, $data)
  {
    parent::__construct($command, $data);
  }

}
