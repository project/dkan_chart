<?php

namespace Drupal\dkan_chart\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\dkan_chart\Service\BarePageRendering;
use Drupal\dkan_chart\Service\DatastoreVisualizationModeller;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\ParameterBag;

class VisualizeController extends ControllerBase {

  /**
   * @var \Drupal\dkan_chart\Service\DatastoreVisualizationModeller
   */
  protected $datastoreVisualizationModeller;

  /**
   * @var \Drupal\dkan_chart\Service\BarePageRendering
   */
  protected $barePageRendering;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @param DatastoreVisualizationModeller $datastoreVisualizationModeller
   */
  public function __construct(DatastoreVisualizationModeller $datastoreVisualizationModeller, BarePageRendering $bare_page_rendering, EntityRepositoryInterface $entity_repository) {
    $this->datastoreVisualizationModeller = $datastoreVisualizationModeller;
    $this->barePageRendering = $bare_page_rendering;
    $this->entityRepository = $entity_repository;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datastore_visualization_modeller'),
      $container->get('dkan_chart.bare_page_rendering'),
      $container->get('entity.repository')
    );
  }

  /**
   * provide the visualization UI (form)
   *
   * @param NodeInterface $node
   * @param $distribution
   * @return array
   *   render array
   */
  public function visualizationUi(NodeInterface $node, $distribution = FALSE, $limit_store = FALSE) {
    $build = [];
    $this->datastoreVisualizationModeller->setNode($node);
    if (count($this->datastoreVisualizationModeller->getDistributionsInDatastore()) === 1 || $distribution) {
      $build['form'] = \Drupal::formBuilder()->getForm('Drupal\dkan_chart\Form\VisualizeAjaxUiForm', $node, $distribution, $limit_store);
      $build['#attached']['library'][] = 'dkan_chart/dkan_chartvisualization';
    }

    return $build;
  }

  /**
   * provide a form for selecting different distributions in datastore
   *
   * @param NodeInterface $node
   * @param $distribution
   * @return array
   *   render array
   */
  public function visualizationUiDistributionSelection(NodeInterface $node, $distribution = FALSE, $visualization_route = 'entity.node.dataset_visualize.distribution') {
    $build = [];
    $this->datastoreVisualizationModeller->setNode($node);
    if (count($this->datastoreVisualizationModeller->getDistributionsInDatastore()) > 1) {
      $build['form'] = \Drupal::formBuilder()->getForm('Drupal\dkan_chart\Form\ChooseDistributionForm', $node, $distribution, $visualization_route);
    }

    return $build;
  }

  /**
   * permission based access check and additional access check
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function visualizeAccess(AccountInterface $account, NodeInterface $node) {
    if (!$account->hasPermission('access chart configuration')) {
      return AccessResult::forbidden();
    }
    return $this->visualizeAccessDataBased($account, $node);
  }

  /**
   * check access for visualization UI
   *   granted if
   *   - node access
   *   - node is of type data and data type dataset
   *   - node (dataset) has any distribution in datastore
   *
   * @param AccountInterface $account
   * @param NodeInterface $node
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function visualizeAccessDataBased(AccountInterface $account, NodeInterface $node) {
    if ($node->getType() === 'data' && $node->access('view')) {
      if ($node->hasField('field_data_type') && !$node->get('field_data_type')->isEmpty() && $node->get('field_data_type')->value === 'dataset') {
        $this->datastoreVisualizationModeller->setNode($node);
        if ($this->datastoreVisualizationModeller->getDistributionsInDatastore()) {
          return AccessResult::allowed();
        }
      }
    }
    return AccessResult::forbidden();
  }

  /**
   * embed Visualization as rendered bare page
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return mixed
   */
  public function embedVisualization(NodeInterface $node) {
    $distribution = \Drupal::request()->get('distribution') ? $this->entityTypeManager()->getStorage('node')->load(\Drupal::request()->get('distribution')) : NULL;
    $limit_store = (bool) \Drupal::request()->get('limit_store');
    $this->datastoreVisualizationModeller->initData($node, $limit_store, $distribution);
    $chart_type = \Drupal::request()->get('chart_type');
    $label = \Drupal::request()->get('label');
    $this->datastoreVisualizationModeller->setChartType($chart_type);
    $this->datastoreVisualizationModeller->setChartDefintionAxesTitle($label);
    $build = [
      '#markup' => '<canvas id="dkanChart"></canvas>',
      '#allowed_tags' => [
        'canvas',
      ],
    ];
    $build['#attached']['library'][] = 'dkan_chart/dkan_chartEmbedding';
    $calculation = \Drupal::request()->get('calculation');
    $data = \Drupal::request()->get('data');
    $series = \Drupal::request()->get('series');
    $filter = \Drupal::request()->get('filter') ?? [];
    $sort = \Drupal::request()->get('sort');
    $this->datastoreVisualizationModeller->setSort($sort);
    $limit = \Drupal::request()->get('limit');
    $this->datastoreVisualizationModeller->setLimit($limit);
    $filter_options = [];
    foreach ($filter as $filter_key => $filter_description) {
      $filter_parts = explode(':', $filter_key);
      $filter_options[$filter_parts[0]][] = $filter_parts[1];
    }
    $build['#attached']['drupalSettings']['chart_dataset'] = $this->datastoreVisualizationModeller->uniqueValuesMultiple($label, array_values($data), $series, $calculation, $filter_options);
    $build['#attached']['drupalSettings']['chart_definition'] = $this->datastoreVisualizationModeller->getChartDefinition();
    $build['#attached']['drupalSettings']['chart_field_options'] = $this->datastoreVisualizationModeller->getFieldsOptions();
    $build['#cache']['contexts'][] = 'url.query_args';
    return $this->barePageRendering->renderBarePage($build, $distribution ? $this->datastoreVisualizationModeller->getDistributionJSON()->data->title : $node->label());
  }

  /**
   * provide link to query datastore with default limit.
   *
   * @param $build
   *
   * @return array|mixed
   */
  public static function limitDatastoreRequest(&$build = []) {
    $request = \Drupal::request();
    $attributes = $request->attributes;
    if ($attributes instanceof ParameterBag) {
      $route = $attributes->get('_route');
      $parameters_input = $attributes->get('_raw_variables');
      if ($parameters_input instanceof InputBag) {
        $parameters = $parameters_input->all();
        $parameters['limit_store'] = true;
        $limit_link = Link::createFromRoute(t('Try with limit'), $route, $parameters);
        $build['failed_to_load'] = [
          '#theme' => 'visualization_failed_to_load',
          '#info' => t('Data failed to load. You can try with limited amount of data again.'),
          '#try_link' => $limit_link->toRenderable()
        ];
      }
    }
    return $build;
  }
}
