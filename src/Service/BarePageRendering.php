<?php

namespace Drupal\dkan_chart\Service;

use Drupal\Core\Render\AttachmentsResponseProcessorInterface;
use Drupal\Core\Render\BareHtmlPageRenderer;
use Drupal\Core\Render\RendererInterface;

class BarePageRendering {
  /**
   * @var RendererInterface
   */
  protected $renderer;

  /**
   * @var AttachmentsResponseProcessorInterface
   */
  protected $attachmentsProcessor;

  /**
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Render\AttachmentsResponseProcessorInterface $attachments_processor
   */
  public function __construct(RendererInterface $renderer, AttachmentsResponseProcessorInterface $attachments_processor) {
    $this->renderer = $renderer;
    $this->attachmentsProcessor = $attachments_processor;
  }

  /**
   * render bare page from $build render array
   *
   * @param $build
   * @param $title
   * @param $page_theme_property
   *
   * @return \Drupal\Core\Render\AttachmentsInterface|\Drupal\Core\Render\HtmlResponse
   */
  public function renderBarePage($build, $title, $page_theme_property = 'markup') {
    $bareHtmlPageRenderer = new BareHtmlPageRenderer($this->renderer, $this->attachmentsProcessor);
    return $bareHtmlPageRenderer->renderBarePage($build, $title, $page_theme_property);
  }
}
