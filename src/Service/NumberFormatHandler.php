<?php

namespace Drupal\dkan_chart\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

// ToDo: Provide a settings form, maybe with multiple thousands separators possible

class NumberFormatHandler
{

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $numberConfig;

  public function __construct(protected ConfigFactoryInterface $configFactory)
  {
    $this->numberConfig = $this->configFactory->get('dkan_chart.number_settings');
  }

  public function handleNumberFormat($value) {
    $value = $this->removeThousandsSeparator($value);
    $value = $this->switchDecimalSeparator($value);
    return floatval($value);
  }

  protected function removeThousandsSeparator($value) {
    if (!empty($this->numberConfig->get('thousands_separator'))) {
      return str_replace($this->numberConfig->get('thousands_separator'), '', $value);
    }
    return $value;
  }

  protected function switchDecimalSeparator($value) {
    if (!empty($this->numberConfig->get('decimal_separator'))) {
      return str_replace($this->numberConfig->get('decimal_separator'), '.', $value);
    }
    return $value;
  }
}
