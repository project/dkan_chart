<?php

namespace Drupal\dkan_chart\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\metastore\MetastoreService;
use Drupal\node\NodeInterface;

class DatastoreVisualizationModeller {

  use StringTranslationTrait;

  /**
   * @var
   *
   * contains the data extracted from datastore API
   */
  protected $data;

  /**
   * @var NodeInterface
   */
  protected $node;

  /**
   * @var NodeInterface|null
   */
  protected $distribution;

  /**
   * @var int
   *
   * distribution id in datastore, defaults to 0 = first distribution
   */
  protected $resource_id = 0;

  /**
   * @var
   *
   * store the fields extracted from datastore API schema
   */
  protected $fields;

  /**
   * @var int|null
   *
   * limiting the datapoints
   */
  protected $limit;

  /**
   * @var string|null
   *
   * sorting the data by labels.
   */
  protected $sort;

  /**
   * @var string
   *
   * the chart type. default to barx.
   */
  protected $chartType;

  /**
   * @var array
   *
   * chart definitions by chart type
   */
  protected $chartDefinition = [
    'barx' => [
      'chart' => 'bar',
      'axis' => 'x',
      'axiskey' => 'y',
      'stacked' => FALSE,
      'title' => [
        'display' => FALSE,
        'font' => [
          'size' => 16
        ]
      ]
    ],
    'bary' => [
      'chart' => 'bar',
      'axis' => 'y',
      'axiskey' => 'x',
      'stacked' => FALSE,
      'title' => [
        'display' => FALSE,
        'font' => [
          'size' => 16
        ]
      ]
    ],
    'barxstack' => [
      'chart' => 'bar',
      'axis' => 'x',
      'axiskey' => 'y',
      'stacked' => TRUE,
      'title' => [
        'display' => FALSE,
        'font' => [
          'size' => 16
        ]
      ]
    ],
    'barystack' => [
      'chart' => 'bar',
      'axis' => 'y',
      'axiskey' => 'x',
      'stacked' => TRUE,
      'title' => [
        'display' => FALSE,
        'font' => [
          'size' => 16
        ]
      ]
    ],
    'pie' => [
      'chart' => 'pie',
    ],
    'doughnut' => [
      'chart' => 'doughnut',
    ],
    'line' => [
      'chart' => 'line',
      'axis' => 'x',
      'axiskey' => 'y',
      'title' => [
        'display' => FALSE,
        'font' => [
          'size' => 16
        ]
      ]
    ],
  ];

  /**
   * @param MetastoreService $metastore
   * @param NumberFormatHandler $numberFormatHandler
   */
  public function __construct(protected MetastoreService $metastore, protected NumberFormatHandler $numberFormatHandler) {

  }

  /**
   * initialize the modeller with dataset node and probably distribution node.
   *
   * @param \Drupal\node\NodeInterface $node
   * @param bool $limit
   * @param \Drupal\node\NodeInterface|NULL $distribution
   *
   * @return void
   */
  public function initData(NodeInterface $node, bool $limit, NodeInterface $distribution = NULL)
  {
    $this->setNode($node);
    $this->setDistribution($distribution);
    $this->setDataFromApi($limit);
    $this->setFields();
  }

  /**
   * set dataset node.
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return void
   */
  public function setNode(NodeInterface $node) {
    $this->node = $node;
  }

  /**
   * set distribution node if any.
   *
   * @param \Drupal\node\NodeInterface|null $distribution
   *
   * @return void
   */
  public function setDistribution(?NodeInterface $distribution) {
    $this->distribution = $distribution;
  }

  /**
   * set data from Datastore API.
   *
   * @param $limit
   *
   * @return void
   */
  protected function setDataFromApi($limit = FALSE)
  {
    if ($this->getDatastoreApiUrl($limit)) {
      $this->data = json_decode(file_get_contents($this->getDatastoreApiUrl($limit)));
    }
  }

  /**
   * set fields from api data schema.
   *
   * @return void
   */
  protected function setFields()
  {
    $this->fields = [];
    $fields = $this->data->schema->{$this->getDistributionId()}->fields ?? [];
    foreach ($fields as $api_name => $field) {
      $this->fields[$api_name] = [
        'description' => $field->description ?? $api_name,
        'type' => $this->estimateFieldType($api_name),
      ];
    }
  }

  /**
   * estimate field type by testing some values from the datastore API.
   *
   * @param $field_name
   *
   * @return string
   */
  protected function estimateFieldType($field_name) {
    $tests = array_rand($this->data->results, count($this->data->results) < 5 ? count($this->data->results): 5);
    $tested = [];
    foreach ($tests as $test) {
      $tested[] = [
        'numeric' => is_numeric($this->numberFormatHandler->handleNumberFormat($this->data->results[$test]->{$field_name})) || empty($this->data->results[$test]->{$field_name}),
        'date' => strtotime($this->data->results[$test]->{$field_name}),
        'leading_numeric' => preg_match('/(?m)^(\d+).*/m', $this->data->results[$test]->{$field_name}) && !is_numeric($this->data->results[$test]->{$field_name}),
      ];
    }
    $numeric = TRUE;
    foreach ($tested as $round) {
      $numeric = $round['numeric'];
      if ($numeric === FALSE) {
        break;
      }
    }
    $leading_numeric = TRUE;
    foreach ($tested as $round) {
      $leading_numeric = $round['leading_numeric'];
      if ($leading_numeric === FALSE) {
        break;
      }
    }
    $date = TRUE;
    $epoch = [];
    foreach ($tested as $round) {
      $date = $round['date'];
      if ($date === FALSE) {
        break;
      }
      $epoch[] = $date === strtotime(0);
    }
    $all_epoch = TRUE;
    foreach ($epoch as $is_epoch) {
      if (!$is_epoch) {
        $all_epoch = FALSE;
        break;
      }
    }
    $date = $date && $all_epoch;

    if ($numeric && $date) {
      return 'numeric-date';
    }
    if ($numeric) {
      return 'numeric';
    }
    if ($date) {
      return 'date';
    }
    if ($leading_numeric) {
      return 'leading-numeric';
    }
    return 'text';
  }


  /**
   * set the chart type.
   *
   * @param string $chart_type
   *
   * @return void
   */
  public function setChartType(string $chart_type)
  {
    $this->chartType = $chart_type;
  }

  /**
   * set the chart axes title
   *
   * @param $title
   *
   * @return void
   */
  public function setChartDefintionAxesTitle($title) {
    if (!empty($this->chartDefinition[$this->getChartType()]['title'])) {
      $this->chartDefinition[$this->getChartType()]['title']['display'] = TRUE;
      $this->chartDefinition[$this->getChartType()]['title']['text'] = $this->getFieldsOptions()[$title];
    }
  }

  /**
   * set chart definition labels.
   *
   * @param array $labels
   *
   * @return void
   */
  protected function setChartDefintionLabels(array $labels)
  {
    $this->chartDefinition['label'] = $labels;
  }

  /**
   * add to chart definition labels.
   *
   * @param string $label
   *
   * @return void
   */
  protected function addChartDefintionLabel(string $label) {
    $this->chartDefinition['label'][] = $label;
  }


  /**
   * sort setter.
   *
   * valid options see @getSortingOptions().
   *
   * @param string $sort
   *
   * @return void
   */
  public function setSort($sort) {
    if (empty($sort)) {
      $this->sort = FALSE;
    }
    else {
      $sort_split = explode(':', $sort);
      $this->sort = [
        'type' => $sort_split[0],
        'order' => $sort_split[1],
      ];
    }
  }

  /**
   * datapoints limit setter.
   *
   * @param int|null $limit
   *
   * @return void
   */
  public function setLimit(?int $limit) {
    $this->limit = $limit;
  }

  public function filterOptions($add_count = TRUE, $slice = 25)
  {
    $filter_options = [];
    $field_options = $this->getFieldsOptions();
    foreach ($field_options as $column_name => $column_description) {
      $filter_options[$column_description] = [];
      foreach ($this->data->results as $result) {
        if (isset($filter_options[$column_description][$column_name . ':' . $result->{$column_name}])) {
          $filter_options[$column_description][$column_name . ':' . $result->{$column_name}]['count'] = $filter_options[$column_description][$column_name . ':' . $result->{$column_name}]['count'] + 1;
        }
        else {
          $filter_options[$column_description][$column_name . ':' . $result->{$column_name}] = [
            'name' => $result->{$column_name},
            'count' => 1
          ];
        }
      }
      uasort($filter_options[$column_description], [$this, 'sortFiltersByCount']);
      $filter_options[$column_description] = $this->prepareFilter($filter_options[$column_description], $add_count, $slice);
    }
    return $filter_options;
  }

  protected function sortFiltersByCount($filterA, $filterB) {
    return $filterB['count'] <=> $filterA['count'];
  }

  protected function prepareFilter($filters, $add_count = TRUE, $slice = 25) {
    $filters = array_slice($filters, 0, $slice);
    foreach ($filters as $filterKey => $filter) {
      $filters[$filterKey] = $filter['name'];
      if ($add_count) {
        $filters[$filterKey] .= ' (' . $filter['count'] . ')';
      }
    }
    return $filters;
  }

  /**
   * start the processing of the data according to chosen settings.
   *
   * split for multiple columns.
   *
   * @param string $column_name
   * @param array $sum_column
   * @param string $dim_column
   * @param string $calc_type
   * @param array $filters
   *
   * @return array
   */

  public function uniqueValuesMultiple(string $column_name, array $sum_column, string $dim_column, string $calc_type, array $filters = []) {
    $datasets = [];
    foreach ($sum_column as $sum) {
      $datasets[$sum] = $this->uniqueValues($column_name, $sum, $dim_column, $filters, $calc_type);
    }
    $chart_def_label = $this->getChartDefinition()['label'];
    $chart_label = [];
    foreach ($sum_column as $sum) {
      $chart_label[$sum] = $chart_def_label;

    }
    $this->setChartDefintionLabels($chart_label);
    return $datasets;
  }

  /**
   * processing of data according to chosen settings.
   *
   * @param string $column_name
   * @param string $sum_column
   * @param string $dim_column
   * @param array $filters
   * @param string $calc_type
   *
   * @return array
   */
  protected function uniqueValues(string $column_name, string $sum_column, string $dim_column, array $filters, string $calc_type)
  {
    if ($dim_column) {
      if ($this->getLimit()) {
        $this->setChartDefintionLabels(array_slice($this->uniqueValuesKeys($dim_column), 0, $this->getLimit()));
      } else {
        $this->setChartDefintionLabels($this->uniqueValuesKeys($dim_column));
      }
    }
    else {
      $this->setChartDefintionLabels(['default']);
    }

    $unique_values = [];

    foreach ($this->data->results as $result) {
      if ($this->filterValues($result, $filters)) {
        $result_dim = $result->{$dim_column} ?? 'default';
        $this->valueCalculation($unique_values, $result, $column_name, $sum_column, $result_dim, $calc_type);
      }
    }
    if ($calc_type === 'avg') {
      foreach ($unique_values as $unique_key => $unique_value) {
        foreach ($unique_value as $uniq_key => $uniq_val) {
          $unique_values[$unique_key][$uniq_key] = $uniq_val['sum'] / $uniq_val['count'];
        }
      }
    }

    arsort($unique_values, SORT_NUMERIC);

    if (!empty($this->getSort())) {
      if ($this->sort['type'] === 'alpha') {
        if ($this->sort['order'] === 'asc') {
          ksort($unique_values, SORT_NATURAL);
        }
        else {
          krsort($unique_values, SORT_NATURAL);
        }
      }
      else {
        uasort($unique_values, [$this, 'sortDatasets']);
      }
    }

    if ($this->getLimit()) {
      $unique_values = array_slice($unique_values, 0, $this->getLimit(), TRUE);
    }

    $datasets = [];

    if ($this->getChartType() === 'pie' || $this->getChartType() === 'doughnut') {
      $this->setChartDefintionLabels([]);
      foreach ($unique_values as $dim => $dataset) {
        $this->addChartDefintionLabel($dim);
        foreach ($dataset as $x => $y) {
          $datasets[$x][$dim] = $y;
        }
      }
      foreach ($unique_values as $unique_key => $unique_value) {
        foreach ($datasets as $dataset_key => $dataset) {
          if (!isset($dataset[$unique_key])) {
            $datasets[$dataset_key][$unique_key] = 0;
          }
        }
      }
      foreach ($datasets as $dataset_key => $dataset) {
        ksort($dataset);
        $datasets[$dataset_key] = $dataset;
      }
      $datasets_plain = [];
      foreach ($datasets as $dim => $dataset) {
        $datasets_plain[$dim] = array_values($dataset);
      }

      $labels = $this->getChartDefinition()['label'];
      sort($labels);
      $this->setChartDefintionLabels($labels);

      return $datasets_plain;
    }
    foreach ($unique_values as $dim => $dataset) {
      $data_object = new \stdClass();
      $data_object->{$this->chartDefinition[$this->getChartType()]['axis']} = (string) $dim;
      foreach ($dataset as $x => $y) {
        $data_object->{(string) $x} = $y;
      }
      $datasets[] = $data_object;
    }
    return $datasets;
  }

  /**
   * key processing helper for labeling.
   *
   * @param string $column_name
   *
   * @return int[]|string[]
   */
  protected function uniqueValuesKeys(string $column_name)
  {
    $unique_values = [];
    foreach ($this->data->results as $result) {
      $unique_values[$result->{$column_name}] = TRUE;
    }
    return array_keys($unique_values);
  }

  /**
   * do the calculation based on aggregation type.
   *
   * @param $unique_values
   * @param $result
   * @param $column_name
   * @param $sum_column
   * @param $result_dim
   * @param $calc_type
   *
   * @return void
   */
  protected function valueCalculation(&$unique_values, $result, $column_name, $sum_column, $result_dim, $calc_type) {
    if (empty($unique_values[$result->{$column_name}][$result_dim])) {
      switch ($calc_type) {
        case 'count':
          $unique_values[$result->{$column_name}][$result_dim] = 1;
          break;
        case 'sum':
        case 'min':
        case 'max':
          $unique_values[$result->{$column_name}][$result_dim] = $this->numberFormatHandler->handleNumberFormat($result->{$sum_column});
          break;
        case 'avg':
          $unique_values[$result->{$column_name}][$result_dim] = [
            'count' => 1,
            'sum' => $this->numberFormatHandler->handleNumberFormat($result->{$sum_column})
          ];
      }
    } else {
      switch ($calc_type) {
        case 'count':
          $unique_values[$result->{$column_name}][$result_dim]++;
          break;
        case 'sum':
          $unique_values[$result->{$column_name}][$result_dim] += $this->numberFormatHandler->handleNumberFormat($result->{$sum_column});
          break;
        case 'min':
          if ($unique_values[$result->{$column_name}][$result_dim] > $this->numberFormatHandler->handleNumberFormat($result->{$sum_column})) {
            $unique_values[$result->{$column_name}][$result_dim] = $this->numberFormatHandler->handleNumberFormat($result->{$sum_column});
          }
          break;
        case 'max':
          if ($unique_values[$result->{$column_name}][$result_dim] < $this->numberFormatHandler->handleNumberFormat($result->{$sum_column})) {
            $unique_values[$result->{$column_name}][$result_dim] = $this->numberFormatHandler->handleNumberFormat($result->{$sum_column});
          }
          break;
        case 'avg':
          $unique_values[$result->{$column_name}][$result_dim]['count']++;
          $unique_values[$result->{$column_name}][$result_dim]['sum'] += $this->numberFormatHandler->handleNumberFormat($result->{$sum_column});
          break;
      }
    }
  }

  /**
   * process filtering.
   *
   * @param object $values
   * @param array $filters
   *
   * @return bool
   */
  protected function filterValues(object $values, array $filters)
  {
    foreach ($filters as $filter_col => $filter_values) {
      if (!in_array($values->{$filter_col}, $filter_values)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * dataset array sorter callback.
   *
   * @param $a
   * @param $b
   *
   * @return int
   */
  protected function sortDatasets($a, $b) {
    switch ($this->sort['type']) {
      case 'numeric':
        if ($this->sort['order'] === 'asc') {
          return array_sum($a) <=> array_sum($b);
        }
        else {
          return array_sum($b) <=> array_sum($a);
        }
    }
    return 0;
  }

  // **Getters**

  /**
   * get all distributions in datastore for this dataset (via initData()).
   *
   * @return array
   * @throws \Drupal\metastore\Exception\InvalidJsonException
   */
  public function getDistributionsInDatastore() {
    $data = $this->metastore->swapReferences($this->metastore->get('dataset', $this->node->uuid()));
    $distributions = $data->{"$.distribution"} ?? [];
    $identifiers = [];
    foreach ($distributions as $dist) {
      try {
        $response =\Drupal::httpClient()->request(
          'GET',
          \Drupal::request()->getSchemeAndHttpHost() . '/api/1/datastore/query/' . $dist['identifier'] . '?limit=1'
        );
        if ($response->getStatusCode() == '200') {
          // ToDo: Refactor to Switch key and value, beware of uses, one is with array_flip
          $identifiers[$dist['data']['title']] = $dist['identifier'];
        }
      }
      catch (\Exception $exception) {
        // Skip this distribution
      }
    }
    return $identifiers;
  }

  /**
   * distribution node getter.
   *
   * @return \Drupal\node\NodeInterface|null
   */
  public function getDistribution() {
    return $this->distribution;
  }

  /**
   * distribution json getter.
   *
   * @return false|mixed
   */
  public function getDistributionJSON() {
    if ($this->distribution && $this->distribution->hasField('field_json_metadata') && !$this->distribution->get('field_json_metadata')->isEmpty()) {
      return json_decode($this->distribution->get('field_json_metadata')->value);
    }
    return FALSE;
  }

  /**
   * distribution id getter.
   *
   * @return mixed
   */
  protected function getDistributionId()
  {
    return $this->data->query->resources[$this->resource_id]->id;
  }

  /**
   * datastore API url getter.
   *
   * @param $limit
   *
   * @return false|string
   * @throws \Drupal\metastore\Exception\InvalidJsonException
   */
  protected function getDatastoreApiUrl($limit = FALSE)
  {
    if ($this->node instanceof NodeInterface) {
      if (!empty($distributions = $this->getDistributionsInDatastore())) {
        $distribution = $this->distribution ? $this->distribution->uuid() : reset($distributions);
        $base_datastore_api_url = \Drupal::request()
            ->getSchemeAndHttpHost() . '/api/1/datastore/query/' . $distribution;
        if ($limit === FALSE) {
          $base_datastore_api_url = $base_datastore_api_url . '?limit=0';
        }
        return $base_datastore_api_url;
      }
    }
    return FALSE;
  }

  /**
   * fields getter.
   *
   * @return mixed
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * chart definition getter.
   *
   * @return array|mixed
   */
  public function getChartDefinition() {
    $this->chartDefinition['type'] = $this->getChartType();
    return $this->chartDefinition;
  }

  /**
   * data results getter.
   *
   * @return mixed
   */
  public function getResults() {
    return $this->data->results;
  }

  /**
   * check for data.
   *
   * @return bool
   */
  public function hasData()
  {
    return !empty($this->data);
  }

  /**
   * chart type getter.
   *
   * @return string
   */
  protected function getChartType() {
    return $this->chartType ?? 'barx';
  }

  /**
   * field definition getter.
   *
   * @param array $type
   *
   * @return array
   */
  public function getFieldsOptions(array $type = []) {
    $fields_options = [];
    foreach ($this->getFields() as $api_field_name => $field) {
      if (empty($type) || in_array($field['type'], $type)) {
        $fields_options[$api_field_name] = $field['description'];
      }
    }
    return $fields_options;
  }

  /**
   * sort getter.
   *
   * valid options see @getSortingOptions().
   *
   * @return mixed
   */
  public function getSort() {
    return $this->sort;
  }

  /**
   * get valid sort options.
   *
   * @return array
   */
  public function getSortingOptions() {
    return [
      'numeric:asc' => $this->t('Numeric ascending'),
      'numeric:desc' => $this->t('Numeric descending'),
      'alpha:asc' => $this->t('Alphabetic ascending'),
      'alpha:desc' => $this->t('Alphabetic descending')
    ];
  }

  /**
   * datapoints limit getter.
   *
   * @return mixed
   */
  public function getLimit() {
    return $this->limit;
  }

  /**
   * get datapoints limit options.
   *
   * @return array
   */
  public function getLimitOptions() {
    return [
      5 => t('5 items'),
      10 => t('10 items'),
      20 => t('20 items'),
      40 => t('40 items'),
      50 => t('50 items'),
      80 => t('80 items'),
      100 => t('100 items'),
    ];
  }

}
