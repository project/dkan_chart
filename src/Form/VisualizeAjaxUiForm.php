<?php

namespace Drupal\dkan_chart\Form;

use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\SettingsCommand;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Url;
use Drupal\dkan_chart\Command\DkanVisualizeCommand;
use Drupal\dkan_chart\Controller\VisualizeController;
use Drupal\dkan_chart\Service\DatastoreVisualizationModeller;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class VisualizeAjaxUiForm extends \Drupal\Core\Form\FormBase {

  /**
   * @var DatastoreVisualizationModeller
   */
  protected $visualizationModeller;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  public function __construct(DatastoreVisualizationModeller $visualization_modeller, EntityRepositoryInterface $entity_repository) {
    $this->visualizationModeller = $visualization_modeller;
    $this->entityRepository = $entity_repository;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('datastore_visualization_modeller'),
      $container->get('entity.repository')
    );
  }

  public function getFormId()
  {
    return 'dkan_visualize_ui';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, NodeInterface $node = NULL, $distribution = FALSE, $limit_store = FALSE)
  {
    $form['mychart'] = [
      '#markup' => '<canvas id="dkanChart"></canvas>',
      '#allowed_tags' => [
        'canvas'
      ]
    ];

    $form['messages'] = [
      '#markup' => '<div class="message-box"></div>'
    ];

    $form['embed_url'] = [
      '#markup' => '<div class="embed-url"></div>'
    ];

    $distribution_node = $distribution ? $this->entityRepository->loadEntityByUuid('node', $distribution) : NULL;
    $this->visualizationModeller->initData($node, $limit_store, $distribution_node);
    if ($limit_store === FALSE && !$this->visualizationModeller->hasData()) {
      VisualizeController::limitDatastoreRequest($form);
      $form['mychart']['#access'] = FALSE;
      return $form;
    }
    if ($this->visualizationModeller->getDistribution()) {
      $form['distribution_label'] = [
        '#type' => 'markup',
        '#markup' => $this->visualizationModeller->getDistributionJSON()->data->title
      ];
    }
    $form['chart_type'] = [
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => [
        'barx' => t('Vertical bars'),
        'bary' => t('Horizontal bars'),
        'barxstack' => t('Vertical bars stacked'),
        'barystack' => t('Horizontal bars stacked'),
        'pie' => t('Pie'),
        'doughnut' => t('Doughnut'),
        'line' => t('Line')
      ],
      '#required' => TRUE,
    ];
    $form['calculation'] = [
      '#type' => 'select',
      '#title' => t('Calculation'),
      '#options' => [
        'sum' => t('Sum'),
        'count' => t('Count'),
        'min' => t('Minimum'),
        'max' => t('Maximum'),
        'avg' => t('Average'),
      ],
      '#required' => TRUE,
      '#default_value' => 'sum'
    ];
    $form['label'] = [
      '#type' => 'select',
      '#title' => t('Label'),
      '#options' => $this->visualizationModeller->getFieldsOptions(),
      '#required' => TRUE,
    ];
    $form['data'] = [
      '#type' => 'select',
      '#title' => t('Data'),
      '#multiple' => TRUE,
      '#options' => $this->visualizationModeller->getFieldsOptions(['numeric-date', 'numeric', 'leading-numeric']),
      '#required' => TRUE,
      '#attributes' => [
        'class' => [
          'choice-select'
        ],
        'data-placeholdervalue' => $this->t('Choose data')
      ]
    ];
    $form['series'] = [
      '#type' => 'select',
      '#title' => t('Series'),
      '#options' => $this->visualizationModeller->getFieldsOptions(),
      '#empty_value' => FALSE
    ];
    $form['filter'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => t('Filter'),
      '#options' => $this->visualizationModeller->filterOptions(),
      '#attributes' => [
        'class' => [
          'choice-select'
        ],
        'data-placeholdervalue' => $this->t('Choose filters'),
        'data-addgroupinsearch' => 'true',
      ]
    ];
    $form['sort'] = [
      '#type' => 'select',
      '#title' => t('Sort'),
      '#empty_value' => FALSE,
      '#options' => $this->visualizationModeller->getSortingOptions(),
      /* not needed anymore, but let it here for reference how #states work ...
      '#states' => [
        'invisible' => [
          'select[name="chart_type"]' => [
            ['value' => 'pie'],
            ['value' => 'doughnut']
          ]
        ]
      ]
      */
    ];
    $form['limit'] = [
      '#type' => 'select',
      '#title' => t('Limit items'),
      '#empty_value' => FALSE,
      '#options' => $this->visualizationModeller->getLimitOptions(),
    ];
    $form['limit_store'] = [
      '#type' => 'hidden',
      '#value' => $limit_store
    ];
    $form['node'] = [
      '#type' => 'hidden',
      '#value' => $node->id()
    ];

    $form['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Visualize'),
      '#ajax' => [
        'callback' => '::visualize'
      ]
    ];

    return $form;
  }

  public function visualize(array $form, \Drupal\Core\Form\FormStateInterface $formState) {
    $response = new \Drupal\Core\Ajax\AjaxResponse();

    if ($formState->getErrors()) {
      $error_content = [
        '#type' => 'status_messages'
      ];
      $response->addCommand(new HtmlCommand('.message-box', $error_content));
      return $response;
    }

    $chart_type = $formState->getValue('chart_type');
    $label = $formState->getValue('label');
    $this->visualizationModeller->setChartType($chart_type);
    $this->visualizationModeller->setChartDefintionAxesTitle($label);
    $calculation = $formState->getValue('calculation');
    $data = $formState->getValue('data');
    $series = $formState->getValue('series');
    $node_id = $formState->getValue('node');
    $filter =  $formState->getValue('filter');
    $sort = $formState->getValue('sort');
    $this->visualizationModeller->setSort($sort);
    $limit = $formState->getValue('limit');
    $this->visualizationModeller->setLimit($limit);
    $limit_store = $formState->getValue('limit_store');
    $filter_options = [];
    foreach ($filter as $filter_key => $filter_description) {
      $filter_parts = explode(':', $filter_key);
      $filter_options[$filter_parts[0]][] = $filter_parts[1];
    }

    $node = Node::load($node_id);
    // Clear message box from subsequent errors
    $response->addCommand(new HtmlCommand('.message-box', []));

    $options = [
      'query' => [
        'vis_type' => 'chartjs',
        'chart_type' => $chart_type,
        'calculation' => $calculation,
        'label' => $label,
        'data' => $data,
        'series' => $series,
        'filter' => $filter,
        'sort' => $sort,
        'limit' => $limit,
        'limit_store' => $limit_store,
        'node' => $node_id
      ],
      'absolute' => TRUE
    ];
    if ($this->visualizationModeller->getDistribution()) {
      $options['query']['distribution'] = $this->visualizationModeller->getDistribution()->id();
    }
    $embed_url = Url::fromRoute('embed.node.dataset_visualize', ['node' => $node_id], $options);
    $embed = [
      '#theme' => 'clipboardjs_textfield',
      '#label' => $this->t('Click to copy'),
      '#value' => urldecode($embed_url->toString()),
      '#alert_style' => 'tooltip', // e.g., 'tooltip', 'alert' or 'none'
      '#alert_text' => $this->t('Copied!'),
    ];
    $response->addCommand(new HtmlCommand('.embed-url', $embed));

    $response->addCommand(new SettingsCommand(['chart_dataset' => null], TRUE));
    $response->addCommand(new SettingsCommand(['chart_dataset' => $this->visualizationModeller->uniqueValuesMultiple($label, array_values($data), $series, $calculation, $filter_options)], TRUE));
    $response->addCommand(new SettingsCommand(['chart_definition' => null], TRUE));
    $response->addCommand(new SettingsCommand(['chart_definition' => $this->visualizationModeller->getChartDefinition()], TRUE));
    $response->addCommand(new SettingsCommand(['chart_field_options' => null], TRUE));
    $response->addCommand(new SettingsCommand(['chart_field_options' => $this->visualizationModeller->getFieldsOptions()], TRUE));
    $response->addCommand(
      new DkanVisualizeCommand('dkanVisualize', [
        'label' => $label,
        'datacol' => $data
      ])
    );

    return $response;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
  {
    // not called, it's Ajax
  }
}
