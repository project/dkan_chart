<?php

namespace Drupal\dkan_chart\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\dkan_chart\Service\DatastoreVisualizationModeller;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChooseDistributionForm extends FormBase {

  /**
   * @var DatastoreVisualizationModeller
   */
  protected $visualizationModeller;

  protected $visualizationRoute;

  public function __construct(DatastoreVisualizationModeller $visualization_modeller) {
    $this->visualizationModeller = $visualization_modeller;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('datastore_visualization_modeller')
    );
  }
  public function getFormID() {
    return 'dkan_choose_distribution_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL, $distribution = FALSE, $visualization_route = 'entity.node.dataset_visualize.distribution')
  {
    $this->visualizationRoute = $visualization_route;
    $this->visualizationModeller->setNode($node);
    $distributions = $this->visualizationModeller->getDistributionsInDatastore();
    $form['distribution'] = [
      '#type' => 'select',
      '#options' => array_flip($distributions),
      '#default_value' => $distribution
    ];
    $form['node'] = [
      '#type' => 'hidden',
      '#value' => $node->id()
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Select'),
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $node = $form_state->getValue('node');
    $distribution = $form_state->getValue('distribution');
    $url = Url::fromRoute($this->visualizationRoute, [
      'node' => $node,
      'distribution' => $distribution,
      'limit_store' => 0
    ]);
    $form_state->setRedirectUrl($url);

  }
}
