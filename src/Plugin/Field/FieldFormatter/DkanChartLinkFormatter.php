<?php

namespace Drupal\dkan_chart\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\dkan_chart\Service\DatastoreVisualizationModeller;
use Drupal\link\LinkItemInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'dkan_chart_link' formatter.
 *
 * @FieldFormatter(
 *   id = "dkan_chart_link",
 *   label = @Translation("DKAN Chart Link"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class DkanChartLinkFormatter extends FormatterBase {

  /**
   * @var \Drupal\dkan_chart\Service\DatastoreVisualizationModeller
   */
  protected $datastoreVisualizationModeller;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DatastoreVisualizationModeller $visualization_modeller, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->datastoreVisualizationModeller = $visualization_modeller;
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('datastore_visualization_modeller'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * visualization types defined by key and callback function
   *
   * @return string[]
   */
  protected function getVisualizationTypes() {
    return [
      'chartjs' => 'webComponentDkanChart',
      'dkan_tables_datatables' => 'webComponentDkanTablesDatatables',
    ];
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $field_name = $items->getFieldDefinition()->getName();
    $target_basename = $entity->getEntityTypeId() . '-' . $entity->id() . '-' . $field_name;

    foreach ($items as $delta => $item) {
      if ($item instanceof LinkItemInterface) {
        $uri = $item->getUrl()->getUri();
        $parsed_uri = UrlHelper::parse($uri);
        $configuration = $parsed_uri['query'];
        $web_comp = FALSE;
        foreach ($this->getVisualizationTypes() as $type => $callback_name) {
          if ($configuration['vis_type'] === $type) {
            $web_comp = $this->{$callback_name}($configuration, $item, $delta, $target_basename);
          }
        }
        if ($web_comp) {
          $element[$delta] = $web_comp;
        }
      }
    }
    return $element;
  }

  protected function webComponentDkanChart($configuration, LinkItemInterface $item, $delta, $target_basename) {
    $node = $this->entityTypeManager->getStorage('node')->load($configuration['node']) ?? FALSE;
    if ($node instanceof NodeInterface) {
      $distribution = $configuration['distribution'] ? $this->entityTypeManager->getStorage('node')->load($configuration['distribution']) : NULL;
      $limit_store = $configuration['limit_store'] ?? FALSE;
      $this->datastoreVisualizationModeller->initData($node, $limit_store, $distribution);
      $chart_type = $configuration['chart_type'] ?? FALSE;
      $label = $configuration['label'] ?? FALSE;
      $this->datastoreVisualizationModeller->setChartType($chart_type);
      $this->datastoreVisualizationModeller->setChartDefintionAxesTitle($label);
      $calculation = $configuration['calculation'];
      $data = $configuration['data'];
      $series = $configuration['series'];
      $filter = $configuration['filter'] ?? [];
      $sort = $configuration['sort'];
      $this->datastoreVisualizationModeller->setSort($sort);
      $limit = $configuration['limit'];
      $this->datastoreVisualizationModeller->setLimit($limit);
      $filter_options = [];
      foreach ($filter as $filter_key => $filter_description) {
        $filter_parts = explode(':', $filter_key);
        $filter_options[$filter_parts[0]][] = $filter_parts[1];
      }
      $comp = [
        '#theme' => 'web_component_dkan_chart',
        '#chart_dataset' => json_encode($this->datastoreVisualizationModeller->uniqueValuesMultiple($label, array_values($data), $series, $calculation, $filter_options)),
        '#chart_definition' => json_encode($this->datastoreVisualizationModeller->getChartDefinition()),
        '#chart_field_options' => json_encode($this->datastoreVisualizationModeller->getFieldsOptions()),
        '#canvas_id' => Html::cleanCssIdentifier($target_basename .  '-' . $delta),
        '#title' => $item->getValue()['title'],
      ];

      return [
        '#theme' => 'web_component_wrapper',
        '#component' => $comp,
        '#label' => [
          '#markup' => $this->getDistributionLabel($distribution)
        ],
        '#embed' => $this->embedButton($item)
      ];
    }
    return FALSE;
  }

  protected function webComponentDkanTablesDatatables($configuration, LinkItemInterface $item, $delta, $target_basename) {
    $node = $this->entityTypeManager->getStorage('node')->load($configuration['node']) ?? FALSE;
    if ($node instanceof NodeInterface) {
      $distribution = $this->entityTypeManager->getStorage('node')->load($configuration['distribution']) ?? NULL;
      $this->datastoreVisualizationModeller->setNode($node);
      $limit_store = $configuration['limit_store'] ?? FALSE;
      $this->datastoreVisualizationModeller->initData($node, $limit_store, $distribution);
      $comp = [
        '#theme' => 'web_component_dkan_tables_datatables',
        '#columns' => json_encode($this->datastoreVisualizationModeller->getFields()),
        '#columns_visible' => $configuration['columns'] ?? NULL,
        '#source' => json_encode($this->datastoreVisualizationModeller->getResults()),
        '#table_id' => Html::cleanCssIdentifier($target_basename . '-' . $delta),
        '#title' => $item->getValue()['title'],
        '#filter_visible' => $configuration['filter_visible'] ?? NULL,
        '#any_string' => $this->t('Any'),
      ];

      return [
        '#theme' => 'web_component_wrapper',
        '#component' => $comp,
        '#label' => [
          '#markup' => $this->getDistributionLabel($distribution)
        ],
        '#embed' => $this->embedButton($item)
      ];
    }
  }

  protected function embedButton(LinkItemInterface $item) {
    return [
      '#type' => 'item',
      '#theme' => 'clipboardjs_textfield',
      '#title' => $this->t('Embed'),
      '#label' => $this->t('Click to copy'),
      '#value' => urldecode( $item->getUrl()->getUri()),
      '#alert_style' => 'tooltip', // e.g., 'tooltip', 'alert' or 'none'
      '#alert_text' => $this->t('Copied!'),
    ];
  }

  protected function getDistributionLabel($distribution) {
    $distributions = array_flip($this->datastoreVisualizationModeller->getDistributionsInDatastore());
    return $distribution instanceof NodeInterface ? $distributions[$distribution->uuid()] : reset($distributions);
  }

}
