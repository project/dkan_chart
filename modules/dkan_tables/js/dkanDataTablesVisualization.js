(function($, Drupal) {

  Drupal.dkanDataTablesVisualization = function(data_columns, data_source, table_id = 'datatable', embed = false) {

    const columns = [];
    for (const [key, value] of Object.entries(data_columns)) {
      columns.push({
        title: value.description,
        footer: 'Filter'
      })
    }

    const table_data = [];
    for (const [rowkey, rowitem] of Object.entries(data_source)) {
      const tablerow = [];
      for (const [rowitemkey, rowitemvalue] of Object.entries(rowitem)) {
        tablerow.push(rowitemvalue)
      }
      table_data.push(tablerow);
    }
    changeDataTableDefaults(embed);
    return new DataTable('#'+table_id, {
      columns: columns,
      data: table_data,
      layout: {
        topStart: null,
        bottomStart: 'pageLength',
        bottomEnd: ['info', 'paging']
      },
      language: {
        "emptyTable":     Drupal.t("No data available in table"),
        "info":           Drupal.t("Showing _START_ to _END_ of _TOTAL_ entries"),
        "infoEmpty":      Drupal.t("Showing 0 to 0 of 0 entries"),
        "infoFiltered":   Drupal.t("(filtered from _MAX_ total entries)"),
        "lengthMenu":     Drupal.t("Show _MENU_ entries"),
        "loadingRecords": Drupal.t("Loading..."),
        "search":         Drupal.t("Search"),
        "zeroRecords":    Drupal.t("No matching records found"),
        "paginate": {
          "first":      Drupal.t("First"),
          "last":       Drupal.t("Last"),
          "next":       Drupal.t("Next"),
          "previous":   Drupal.t("Previous")
        },
        "aria": {
          "orderable":  Drupal.t("Order by this column"),
          "orderableReverse": Drupal.t("Reverse order this column")
        }
      }
    });
  }

  Drupal.dkanDataTablesFilter = function (table, any_string) {
    table.columns().flatten().each( function ( colIdx ) {
      // Create the select list and search operation
      var select = $('<select>')
        .appendTo(
          table.column(colIdx).footer()
        )
        .on( 'change', function () {
          table
            .column( colIdx )
            .search( $(this).val() )
            .draw();
        } );
      select.append( $('<option value="">'+any_string+'</option>') );
      // Get the search data for the first column and add to the select list
      table
        .column( colIdx )
        .cache( 'search' )
        .sort()
        .unique()
        .each( function ( d ) {
          select.append( $('<option value="'+d+'">'+d+'</option>') );
        } );
    } );
  }

  function changeDataTableDefaults(embed) {
    if ($.fn.dataTable.defaults.renderer === 'bootstrap') {
      if (!embed) {
        $.fn.dataTableExt.classes.container = $.fn.dataTableExt.classes.container + ' container';
      }
      else {
        $.fn.dataTableExt.classes.container = $.fn.dataTableExt.classes.container + ' container-fluid';
      }
      $.fn.dataTableExt.classes.search.input = 'form-control';
      $.fn.dataTableExt.classes.length.select = 'form-select form-control';
    }
  }

}(jQuery, Drupal, drupalSettings));
