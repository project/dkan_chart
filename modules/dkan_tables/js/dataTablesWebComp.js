(function($, Drupal) {

  customElements.define("dkan-data-tables", class extends HTMLElement {
    connectedCallback() {
      let data_columns = JSON.parse(this.getAttribute('data-columns'));
      let data_source = JSON.parse(this.getAttribute('data-source'));
      let table_id = this.getAttribute('data-table-id');
      let columns_visible = this.getAttribute('data-columns-visible');
      Drupal.dkanDataTablesVisualization(data_columns, data_source, table_id);

      if (columns_visible) {
        columns_visible = columns_visible.split(',');
        new DataTable.Api('#' + table_id);
        columns_visible.forEach(function (table_column, index) {
          $('#'+(table_id)).DataTable().column(index).visible(table_column === 'true');
        });
      }

      let filter_visible = this.getAttribute('data-filter-visible');
      let any_string = this.getAttribute('data-any-string') ?? '-';
      if (filter_visible) {
        Drupal.dkanDataTablesFilter($('#'+(table_id)).DataTable(), any_string);
      }
      else {
        $('#'+(table_id)).DataTable().table().footer().style.display = 'none';
      }
    }
  });

} (jQuery, Drupal));
