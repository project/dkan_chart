(function($, Drupal, drupalSettings) {

  let new_table = Drupal.dkanDataTablesVisualization(drupalSettings.dkan_tables.columns, drupalSettings.dkan_tables.source, 'datatable', drupalSettings.dkan_tables.embed);
  Drupal.dkanDataTablesFilter(new_table, drupalSettings.dkan_tables.any_string);

  Drupal.behaviors.DatatablesSelectColumnsBehavior = {
    attach: function (context, settings) {
      new DataTable.Api( '#datatable' );
      let tableColumnVisible = document.querySelectorAll(".table-column-visible");
      let columns_visible = drupalSettings.dkan_tables.columns_visible ? drupalSettings.dkan_tables.columns_visible.split(',') : false;
      tableColumnVisible.forEach(function(checkbox) {
        if (columns_visible) {
          $('#datatable').DataTable().column(checkbox.value).visible(columns_visible[checkbox.value] === 'true');
          checkbox.checked = columns_visible[checkbox.value] === 'true';
        }
        checkbox.addEventListener('click', function(event){
          $('#datatable').DataTable().column(event.target.value).visible(event.target.checked);

          if ($('input.clipboardjs').length) {
            let url = new URL($('input.clipboardjs').val());
            let columns = $('#datatable').DataTable().columns()[0];
            let columns_param = url.searchParams.get('columns');
            let col_param = [];
            if (columns_param === null) {
              columns.forEach(function (col, index) {
                col_param[col] = true;
              });
            } else {
              col_param = columns_param.split(',');
            }
            col_param[event.target.value] = event.target.checked;
            url.searchParams.set('columns', col_param);
            $('input.clipboardjs').val(url.toString());
          }
        });
      });

      let table = $('#datatable').DataTable();
      let tableFilterVisible = document.querySelectorAll(".table-filter-visible");
      let filter_visible = drupalSettings.dkan_tables.filter_visible;

      tableFilterVisible.forEach(function(checkbox) {
        checkbox.checked = filter_visible === 'true';
        table.table().footer().style.display = checkbox.checked ? '' : 'none';
        checkbox.addEventListener('click', function(event){
          table.table().footer().style.display = event.target.checked ? '' : 'none';

          if ($('input.clipboardjs').length) {
            let url = new URL($('input.clipboardjs').val());
            let filter_param = url.searchParams.get('filter_visible');
            url.searchParams.set('filter_visible', event.target.checked);
            $('input.clipboardjs').val(url.toString());
          }
        });
      });

    }
  };


}(jQuery, Drupal, drupalSettings));
