(function($, Drupal, drupalSettings) {

  const columns = [];
  for (const [key, value] of Object.entries(drupalSettings.dkan_tables.columns)) {
    columns.push({
      prop: key,
      name: value.description
    })
  }
  const grid = document.querySelector('revo-grid');
  const items = drupalSettings.dkan_tables.source;
  grid.columns = columns;
  grid.source = items;

}(jQuery, Drupal, drupalSettings));
