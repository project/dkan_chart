<?php

namespace Drupal\dkan_tables\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

class VisualizeController extends \Drupal\dkan_chart\Controller\VisualizeController {

  /**
   * @var \Drupal\node\NodeInterface|null
   */
  protected ?NodeInterface $distribution;

  /**
   * init DatastoreVisualizationModeller.
   *
   * @param \Drupal\node\NodeInterface $node
   * @param $distribution
   * @param $limit_store
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\metastore\Exception\InvalidJsonException
   */
  protected function initDatastoreVisualization(NodeInterface $node, $distribution = NULL, $limit_store = FALSE) {
    $this->datastoreVisualizationModeller->setNode($node);
    $distributions_in_datastore = $this->datastoreVisualizationModeller->getDistributionsInDatastore();
    if (!empty($distributions_in_datastore)) {
      if (empty($distribution)) {
        $distribution = reset($distributions_in_datastore);
      }
      $distribution = $distribution ? $this->entityRepository->loadEntityByUuid('node', $distribution) : NULL;
      if ($distribution instanceof NodeInterface) {
        $this->distribution = $distribution;
      }
      $this->datastoreVisualizationModeller->initData($node, $limit_store, $this->distribution);
    }
  }

  /**
   * proxy getter on DatastoreVisualizationModeller
   *
   * @return bool
   */
  public function dataStoreVisualizationModellerHasData() {
    return $this->datastoreVisualizationModeller->hasData();
  }

  /**
   * embed Visualization as rendered bare page
   *
   * @param \Drupal\node\NodeInterface $node
   * @param $distribution
   *
   * @return \Drupal\Core\Render\AttachmentsInterface|\Drupal\Core\Render\HtmlResponse
   */
  public function embedVisualizationUiTable(NodeInterface $node, $distribution = NULL) {
    $columns = \Drupal::request()->get('columns');
    $filter_visible = \Drupal::request()->get('filter_visible');
    $limit_store = \Drupal::request()->get('limit_store');
    $build = $this->visualizationUiTable($node, $distribution, $limit_store, TRUE);
    $build['#attached']['drupalSettings']['dkan_tables']['columns_visible'] = $columns;
    $build['#attached']['drupalSettings']['dkan_tables']['filter_visible'] = $filter_visible;
    $build['#cache']['contexts'][] = 'url.query_args';
    return $this->barePageRendering->renderBarePage($build, $distribution ? $this->datastoreVisualizationModeller->getDistributionJSON()->data->title : $node->label());
  }

  /**
   * provide table visualization via js table plugin
   *
   * @param \Drupal\node\NodeInterface $node
   * @param bool $distribution
   * @param bool $embed
   * @param bool $full
   *
   * @return array
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\metastore\Exception\InvalidJsonException
   */
  public function visualizationUiTable(NodeInterface $node, $distribution = FALSE, $limit_store = FALSE, $embed = FALSE, $full = FALSE) {
    $this->initDatastoreVisualization($node, $distribution, $limit_store);
    if ($limit_store === FALSE && !$this->datastoreVisualizationModeller->hasData()) {
      $build = [];
      return \Drupal\dkan_chart\Controller\VisualizeController::limitDatastoreRequest($build);
    }
    $build = [
      '#theme' => 'dkan_tables_ui_wrapper'
    ];
    if ($this->distribution instanceof NodeInterface) {
      $distribution_label = array_flip($this->datastoreVisualizationModeller->getDistributionsInDatastore())[$this->distribution->uuid()];
    }
    if (!empty($distribution_label)) {
      $build['#headline'] = [
        '#markup' => $distribution_label
      ];
    }
    $table_plugin = \Drupal::config('dkan_tables.settings')->get('table_plugin') ?? 'datatables';
    $build['#table'] = [
      '#theme' => $table_plugin
    ];
    // select the columns to show
    if ($table_plugin === 'datatables') {
      $build['#table']['#style'] = 'default';
      $build['#table']['#embed'] = $embed;
      if ($full) {
        $build += $this->visualizationUiTableConfiguration();
      }
    }
    $build['#attached']['drupalSettings']['dkan_tables']['columns'] = $this->datastoreVisualizationModeller->getFields();
    $build['#attached']['drupalSettings']['dkan_tables']['source'] = $this->datastoreVisualizationModeller->getResults();
    $build['#attached']['drupalSettings']['dkan_tables']['any_string'] = $this->t('Any');
    $build['#attached']['drupalSettings']['dkan_tables']['embed'] = $embed;
    if ($table_plugin !== 'datatables') {
      $build['#attached']['library'][] = 'dkan_tables/dkan_tablevisualization_' . $table_plugin;
    }
    if (!$embed) {
      $options = [
        'query' => [
          'vis_type' => 'dkan_tables_' . $table_plugin,
          'node' => $node->id(),
          'distribution' => $distribution instanceof NodeInterface ? $distribution->id() : NULL,
          'limit_store' => $limit_store
        ],
        'absolute' => TRUE
      ];
      $embed_url = Url::fromRoute('embed.node.dataset_tables', [
        'node' => $node->id(),
        'distribution' => $distribution instanceof NodeInterface ? $distribution->uuid() : NULL
      ], $options);

      $build['#embed'] = [
        '#type' => 'item',
        '#theme' => 'clipboardjs_textfield',
        '#title' => $this->t('Embed'),
        '#label' => $this->t('Click to copy'),
        '#value' => urldecode($embed_url->toString()),
        '#alert_style' => 'tooltip', // e.g., 'tooltip', 'alert' or 'none'
        '#alert_text' => $this->t('Copied!'),
      ];
    }
    return $build;
  }

  /**
   * only the filtering/configuration options for a datatables.js based table
   * visualization, does usually only make sense together with the table
   * visualization
   *
   * @param \Drupal\node\NodeInterface|null $node
   * @param $distribution
   *
   * @return array
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\metastore\Exception\InvalidJsonException
   */
  public function visualizationUiTableConfiguration(?NodeInterface $node = NULL, $distribution = FALSE, $limit_store = FALSE) {
    if (empty($this->distribution)) {
      $this->initDatastoreVisualization($node, $distribution, $limit_store);
    }
    if ($limit_store === FALSE && !$this->datastoreVisualizationModeller->hasData()) {
      $build = [];
      \Drupal\dkan_chart\Controller\VisualizeController::limitDatastoreRequest($build);
      return $build;
    }
    $build['#table_columns'] = [
      '#type' => 'details',
      '#title' => t('Select columns to show.')
    ];
    $field_count = 0;
    foreach ($this->datastoreVisualizationModeller->getFields() as $field => $field_info) {
      $build['#table_columns'][$field] = [
        '#type' => 'checkbox',
        '#title' => $field_info['description'],
        '#default_value' => TRUE,
        '#return_value' => $field_count,
        '#attributes' => [
          'class' => [
            'table-column-visible'
          ],
          'checked' => TRUE
        ]
      ];
      $field_count++;
    }
    $build['#table_filter'][$field] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show filters'),
      '#default_value' => FALSE,
      '#return_value' => TRUE,
      '#attributes' => [
        'class' => [
          'table-filter-visible'
        ],
      ]
    ];
    return $build;
  }

  public function visualizeAccess(AccountInterface $account, NodeInterface $node) {
    if (!$account->hasPermission('access table configuration')) {
      return AccessResult::forbidden();
    }
    return parent::visualizeAccessDataBased($account, $node);
  }

}
