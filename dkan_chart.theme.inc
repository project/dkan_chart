<?php

/**
 * Implements hook_preprocess_HOOK().
 */
function dkan_chart_preprocess_node__dkan__visualize(&$variables) {
  $variables['page'] = TRUE;
}
