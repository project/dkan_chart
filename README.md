# DKAN Charts

Chart/Table visualization for [DKANv2](https://github.com/GetDKAN/dkan) Datastore API

## Installation

### clipboard.js

The [clipboardjs](https://www.drupal.org/project/clipboardjs) module is used. You need
to take care of installing the clipboard.js library. See 
[README](https://git.drupalcode.org/project/clipboardjs/-/blob/2.0.x/README.md?ref_type=heads#installation)

## Configuration

### Number format settings

You can provide a decimal separator and a thousands separator by config setting.

`$config['dkan_chart.number_settings'] = [
  'decimal_separator' => ',',
  'thousands_separator' => '.',
]`

The decimal separator will be switched to __full stop__. The thousands separator will be removed.

This may help outputting the visualizations. 

## Features

Provide chart visualization features for data from DKANv2 Datastore API, currently based on [chartjs](https://www.chartjs.org/)

There will be a tab "Visualization" on each dataset node that has distribution/s in the datastore. On this tab different visualizations can be arranged for this distribution.

Using [Choices-js](https://github.com/Choices-js/Choices) for some multivalue select fields in the UI of the visualizations (for the time being, may change to Select2 or Selectize later if necessary. Choices-js does not need jQuery, which makes it kind of sympathetic as Drupal is also slightly moving away from jQuery).

### Feature Spreadsheet tables

By enabling the submodule dkan_tables a spreadsheet like output of the data
with [Datatables](https://datatables.net/) (Default) or [Revolist](https://github.com/revolist/revogrid) (optional, now deprecated, will not receive new features currently) is possible

(changed via setting table_plugin in dkan_tables.settings.yml)

## Remarks

JS Libraries plugin-autocolors and choices are patched:

### plugin-autocolors:
https://github.com/stefan-korn/chartjs-plugin-autocolors/tree/data-mode-index-and-push-order

### choices:
https://github.com/Choices-js/Choices/pull/1152
