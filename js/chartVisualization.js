(function($, Drupal, drupalSettings) {

  // The function name is prototyped as part of the Drupal.AjaxCommands namespace, adding to the commands:
  Drupal.AjaxCommands.prototype.dkanVisualize = function(ajax, response, status) {

    Drupal.dkanchartcreation(drupalSettings.chart_definition, drupalSettings.chart_field_options, drupalSettings.chart_dataset);

  };

}(jQuery, Drupal, drupalSettings));



