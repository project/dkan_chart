(function(Drupal) {

  Drupal.dkanchartcreation = function(chart_definition, chart_field_options, chart_dataset, chartid = 'dkanChart') {
    // The value passed in the Ajax callback function will be available inside the
    // response object.
    let chartStatus = Chart.getChart(chartid); // <canvas> id
    if (chartStatus !== undefined) {
      chartStatus.destroy();
    }

    let datasets = [];
    switch (chart_definition[chart_definition.type].chart) {
      case 'bar':
      case 'line':
        for (const [dataKey, dataValue] of Object.entries(chart_definition.label)) {
          dataValue.forEach((varyKey, i) => buildDatasets(dataKey, varyKey, datasets, chart_definition, chart_field_options, chart_dataset));
        }
        break;
      case 'pie':
      case 'doughnut':
        for (const [dataKey, dataValue] of Object.entries(chart_dataset)) {
          Object.keys(dataValue).forEach((key, i) => datasets.push({
            data: chart_dataset[dataKey][key],
            label: build_label(dataKey, key, chart_field_options)
          }));
        }
        break;
    }
    const ctx = document.getElementById(chartid);
    const dkanchart = new Chart(ctx, chartOptions(datasets, chart_definition));
  }

  function buildDatasets(label, add_label, datasets, chart_definition, chart_field_options, chart_dataset) {
    switch (chart_definition[chart_definition.type].chart) {
      case 'bar':
      case 'line':
        // remove labels for empty data
        let push = chart_dataset[label].some(function(obj) {
          return obj[add_label.toString()];
        });
        if (push) {
          datasets.push(
            {
              axis: chart_definition[chart_definition.type].axis,
              data: chart_dataset[label],
              label: build_label(label, add_label, chart_field_options),
              borderWidth: 1,
              parsing: {
                [chart_definition[chart_definition.type].axiskey.toString() + 'AxisKey']: add_label.toString(),
              }
            },
          );
        }
        break;
    }
  }

  function chartOptions(datasets, chart_definition) {
    switch (chart_definition[chart_definition.type].chart) {
      case 'bar':
      case 'line':
        return {
          type: chart_definition[chart_definition.type].chart,
          data: {
            datasets: datasets,
          },
          options: {
            indexAxis: chart_definition[chart_definition.type].axis,
            scales: {
              [chart_definition[chart_definition.type].axiskey.toString()]: {
                stacked: chart_definition[chart_definition.type].stacked
              },
              [chart_definition[chart_definition.type].axis.toString()]: {
                stacked: chart_definition[chart_definition.type].stacked,
                title: chart_definition[chart_definition.type].title ?? false
              },
            }
          },
        };
        break;
      case 'pie':
      case 'doughnut':
        const autocolors = window['chartjs-plugin-autocolors'];
        return {
          type: chart_definition[chart_definition.type].chart,
          data: {
            datasets: datasets,
            labels: chart_definition.label[Object.keys(chart_definition.label)[0]],
          },
          plugins: [
            autocolors
          ],
          options: {
            plugins: {
              colors: {
                enabled: false
              },
              autocolors: {
                mode: 'data',
                customize(context) {
                  const colors = context.colors;
                  if (context.datasetIndex > 0) {
                    return {
                      background: context.chart.config._config.data.datasets[0].backgroundColor[context.dataIndex],
                      border: context.chart.config._config.data.datasets[0].borderColor[context.dataIndex],
                    }
                  }
                  return {
                    background: colors.background,
                    border: colors.border
                  };
                }
              }
            }
          }
        };
        break;
    }
  }

  function build_label(label, add_label, field_options) {
    if (add_label !== 'default') {
      return field_options[label].toString() + ' (' + add_label + ')';
    }
    return field_options[label].toString();
  }

}(Drupal));
