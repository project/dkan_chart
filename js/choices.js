(function (Drupal, once) {
  Drupal.behaviors.choicesSelect = {
    attach: (context, settings) => {
      once("choices-selects-initialized", ".choice-select", context).forEach(
        (select) => {
          new Choices(select, {
            removeItemButton: true,
            placeholderValue: select.dataset.placeholdervalue,
            appendGroupInSearch: select.dataset.addgroupinsearch ?? false,
            resetScrollPosition: false,
            loadingText: Drupal.t('Loading...'),
            noResultsText: Drupal.t('No results found'),
            noChoicesText: Drupal.t('No choices to choose from'),
            itemSelectText: Drupal.t('Press to select'),
            uniqueItemText: Drupal.t('Only unique values can be added'),
            customAddItemText: Drupal.t('Only values matching specific conditions can be added'),
          });
        }
      );
    },
  };
})(Drupal, once);
