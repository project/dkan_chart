(function(Drupal, drupalSettings) {

  Drupal.dkanchartcreation(drupalSettings.chart_definition, drupalSettings.chart_field_options, drupalSettings.chart_dataset);

} (Drupal, drupalSettings));
