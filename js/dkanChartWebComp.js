(function(Drupal) {

  customElements.define("dkan-chart", class extends HTMLElement {
    connectedCallback() {
      let chart_definition = JSON.parse(this.getAttribute('data-chart-definition'));
      let chart_field_options = JSON.parse(this.getAttribute('data-chart-field-options'));
      let chart_dataset = JSON.parse(this.getAttribute('data-chart-dataset'));
      Drupal.dkanchartcreation(chart_definition, chart_field_options, chart_dataset, this.getAttribute('data-canvas-id'));
    }
  });

} (Drupal));

